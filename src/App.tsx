import UserState from "./context/users/UserState";
import { Navigation } from "./routes/Navigation";
import "./index.css";

const App = () => {
  return (
    <>
      <UserState>
        <Navigation />
      </UserState>
    </>
  );
};

export default App;
