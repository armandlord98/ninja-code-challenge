import { UserForm } from "../components";

export const CreateUser = () => {
  return (
    <div>
      <UserForm />
    </div>
  );
};
