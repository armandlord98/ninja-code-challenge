import { UsersList } from "../components";
import { GiNinjaHeroicStance } from "react-icons/gi";

export const Home = () => {
  return (
    <>
      <h1
        id="title-home"
        className="text-3xl text-center flex font-bold text-zinc-400 my-[2rem] sm:text-5xl"
      >
        <GiNinjaHeroicStance />
        Ninjas disponibles
      </h1>
      <UsersList />
    </>
  );
};
