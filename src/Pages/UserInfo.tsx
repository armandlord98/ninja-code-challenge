import { useContext, useEffect } from "react";
import { UserContext } from "../context/users/UserContext";
import { useParams } from "react-router-dom";
import { UserDetail } from "../components/UserDetail";
import { GiNinjaStar } from "react-icons/gi";
import { encryptedNinjaName } from "../utils/encryptedNinjaName";

export const UserInfo = () => {
  const { state, getUserById, deleteUser, updateUser } =
    useContext(UserContext);
  const { id } = useParams();

  useEffect(() => {
    getUserById(`${id}`);
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <h1 className="text-3xl text-center flex font-bold mt-[2rem] text-zinc-400 sm:text-5xl">
        <GiNinjaStar />
        {state.userById.firstname}
      </h1>
      <small className="mb-[2rem] text-center text-l text-zinc-400">
        {encryptedNinjaName(
          state?.userById?.firstname,
          state?.userById?.lastname
        )}
      </small>
      <h1 className="text-zinc-400"></h1>
      <UserDetail
        user={state.userById}
        deleteUser={deleteUser}
        updateUser={updateUser}
      />
    </>
  );
};
