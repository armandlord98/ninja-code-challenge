const URL = "http://localhost:5173";

describe("src/Pages/Home.tsx", () => {
  it("should render the home page", () => {
    cy.visit(URL);
    cy.get("#title-home").should("contain", "Ninjas disponibles");
    cy.wait(1000);
  });

  it("should create a new user", () => {
    cy.visit(URL + "/create-user");
    cy.get("#firstname").type("Armandev");
    cy.get("#lastname").type("Pérez");
    cy.get("#email").type("armandlord98@gmail.com");
    cy.get("#birthDate").type("1998-02-16");
    cy.get("#street").type("Calle 13");
    cy.get("#city").type("De la Esperanza");
    cy.get("#country").type("Konha");
    cy.get("#postalcode").type("12345");
    cy.get("#createNinja").click();
    cy.wait(1000);
  });

  it("should go to the detail user", () => {
    cy.visit(URL);
    cy.get(".grid > :nth-child(1) > a").click();
    cy.get("#editNinja").should("be.visible").click();
    cy.get(".ReactModal__Overlay").click(0, 0);

    cy.get("#deleteNinja").should("be.visible").click();
    cy.get(".swal2-cancel").should("be.visible").click();
    cy.get("#deleteNinja").should("be.visible").click();
    cy.get(".swal2-confirm").should("be.visible").click();
    cy.get(".swal2-confirm").should("be.visible").click();
  });
});
