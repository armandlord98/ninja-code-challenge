import { encryptedNinjaName } from "../../../utils/encryptedNinjaName";

describe("src/utils/encryptedNinjaName.ts", () => {
  it("Should encrypt the ninja's name", () => {
    // ARRANGE
    const firstName = " Armando";
    const lastName = " Pérez";
    // ACT
    const encryptedName = encryptedNinjaName(firstName, lastName);
    // ASSERT
    expect(encryptedName).toBe("a***p");
  });
});
