// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// const apiKey = process.env.REACT_APP_FIREBASE_API_KEY
// const authDomain= process.env.REACT_APP_FIREBASE_AUTH_DOMAIN
// const projectId = process.env.REACT_APP_FIREBASE_PROJECT_ID
// const storageBucket = process.env.REACT_APP_FIREBASE_STORAGE_BUCKET
// const messagingSenderId = process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID
// const appId = process.env.REACT_APP_FIREBASE_APP_ID
// Your web app's Firebase configuration

// Dejo las varibales para que puedan levantar el repo y probarlo
const firebaseConfig = {
  apiKey: "AIzaSyA7GFLGyJi18aHkY0BBMTFehVcAami2kco",
  authDomain: "ninja-code-challenge.firebaseapp.com",
  projectId: "ninja-code-challenge",
  storageBucket: "ninja-code-challenge.appspot.com",
  messagingSenderId: "681096264666",
  appId: "1:681096264666:web:76cb0841796716eb0cdee3",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
