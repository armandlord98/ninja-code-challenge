export const encryptedNinjaName = (firstName: string, lastName: string) => {
  const firstLetter = firstName?.trim().charAt(0).toLocaleLowerCase();
  const lastLetter = lastName?.trim().charAt(0).toLocaleLowerCase();

  const encryptedName = `${firstLetter}***${lastLetter}`;

  return encryptedName;
};
