import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { routes } from "./routes";
import { Footer, Header } from "../components";

export const Navigation = () => {
  return (
    <BrowserRouter>
      <div className="bg-zinc-800 h-full w-screen flex flex-col items-center">
        <Header routes={routes} />
        <Routes>
          {routes.map(({ path, Component }) => (
            <Route key={path} path={path} element={<Component />}></Route>
          ))}
          <Route path="/*" element={<Navigate to={routes[0].to} />}></Route>
        </Routes>
      </div>
      <Footer />
    </BrowserRouter>
  );
};
