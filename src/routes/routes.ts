import { CreateUser, Home, UserInfo } from "../Pages";

type ReactElement = () => JSX.Element;

export interface Route {
  to: string;
  path: string;
  Component: ReactElement;
  name: string;
}

export const routes: Route[] = [
  {
    to: "/",
    path: "/",
    Component: Home,
    name: "Home",
  },
  {
    to: "/create-user",
    path: "/create-user",
    Component: CreateUser,
    name: "Crea un usuario",
  },
  {
    to: "/user/:id",
    path: "/user/:id",
    Component: UserInfo,
    name: "User Info",
  },
];
