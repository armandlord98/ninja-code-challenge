import { FC } from "react";
import { Users } from "../interfaces/UsersInterface";
import { Link } from "react-router-dom";

interface Props {
  user: Users;
}
export const User: FC<Props> = ({ user }) => {
  const { firstname, lastname, email, birthDate, address } = user;

  return (
    <div className="card w-18 m-1 rounded bg-gray-200 p-3 flex flex-col">
      <Link to={`/user/${user.id}`}>
        <h1>
          <b>Nombre:</b> {firstname} {lastname}
        </h1>
        <b>
          Contacto: <small>{email}</small>
        </b>
        <br />
        <b>Información personal:</b>
        <div className="card w-18 m-1 border-1 rounded border-gray-400 bg-gray-300 p-3 flex flex-col">
          <small>
            <b>Cumpleaños: </b>
            {birthDate}
          </small>
          <small>
            <b>Ciudad: </b>
            {address.city}
          </small>
          <small>
            <b>Calle: </b>
            {address.street}
          </small>
          <small>
            <b>C.P.: </b>
            {address.postalcode}
          </small>
        </div>
      </Link>
    </div>
  );
};
