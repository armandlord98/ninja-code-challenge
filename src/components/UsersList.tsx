import { useContext, useEffect } from "react";
import { UserContext } from "../context/users/UserContext";
import { Users } from "../interfaces/UsersInterface";
import { User } from "./User";
import { Link } from "react-router-dom";

export const UsersList = () => {
  const { state, getUsers } = useContext(UserContext);

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 mt-4 mb-4">
      {state.users.length > 0 ? (
        state.users.map((user: Users) => <User key={user.id} user={user} />)
      ) : (
        <div
          style={{
            height: "calc(100vh - 192px)",
          }}
          className="flex justify-center items-center w-screen"
        >
          <span className="bg-cyan-100 h-[10rem] rounded-xl p-6 flex flex-col items-center justify-between text-center">
            <h1 className="text-2xl">No hay Ninjas Disponibles</h1>
            <small>
              Puedes crear un nuevo Ninja haciendo click en el botón de abajo
            </small>
            <Link to="/create-user">
              <button className="bg-green-600 p-1 rounded text-white mt-2">
                Crear Ninja
              </button>
            </Link>
          </span>
        </div>
      )}
    </div>
  );
};
