import { FC, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import Modal from "react-modal";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Toaster, toast } from "sonner";
import { Users } from "../interfaces/UsersInterface";

interface Props {
  user: Users;
  deleteUser: (id: string) => void;
  updateUser: (user: any) => void;
}

export const UserDetail: FC<Props> = ({ user, deleteUser, updateUser }) => {
  const navigate = useNavigate();
  const [openModal, setOpenModal] = useState<boolean>(false);

  let initialValues: Users = {
    id: 0,
    firstname: "",
    lastname: "",
    email: "",
    birthDate: "",
    address: {
      id: 0,
      street: "",
      city: "",
      country: "",
      postalcode: "",
    },
  };

  useEffect(() => {
    initialValues = user;
    return () => {
      initialValues.id = 0;
      initialValues.firstname = "";
      initialValues.lastname = "";
      initialValues.email = "";
      initialValues.birthDate = "";
      initialValues.address = {
        id: 0,
        street: "",
        city: "",
        country: "",
        postalcode: "",
      };
    };
  }, [user]);

  const { handleSubmit, getFieldProps, errors, touched } = useFormik({
    initialValues,
    onSubmit: (values) => {
      updateUser(values);
      toast.success("Ninja actualizado con exito");
      setTimeout(() => {
        navigate("/");
      }, 2000);
    },
    validationSchema: Yup.object({
      firstname: Yup.string().required("Nombre es requerido"),
      lastname: Yup.string().required("Apellido es requerido"),
      email: Yup.string()
        .email("Email no valido")
        .required("Email es requerido"),
      birthDate: Yup.string().required("Fecha de nacimiento es requerido"),
      address: Yup.object({
        street: Yup.string().required("Calle es requerido"),
        city: Yup.string().required("Ciuadad es requerido"),
        country: Yup.string().required("Pais es requerido"),
        postalcode: Yup.string().required("Codigo postal es requerido"),
      }),
    }),
  });

  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };
  Modal.setAppElement("#root");

  const handleDelete = (id: string) => {
    Swal.fire({
      title: "¿Estás seguro?",
      text: "No podrás revertir esto!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sí, bórralo!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Borrado!", "El usuario ha sido borrado.", "success");
        deleteUser(id);
        navigate("/");
      }
    });
  };

  return (
    <div
      style={{
        height: "calc(100vh - 124px)",
      }}
      className="flex flex-col items-center  w-full"
    >
      <Toaster />
      <span className="flex flex-col items-center sm:w-1/2 xl:w-1/2 border-1 rounded-xl border-gray-800 bg-gray-200 p-3 shadow-lg ">
        <div className="w-full flex justify-end">
          <button
            id="editNinja"
            className="bg-blue-600 p-1 rounded text-white mr-2"
            onClick={() => setOpenModal((prev) => !prev)}
          >
            Editar
          </button>
          <button
            id="deleteNinja"
            className="bg-red-600 p-1 rounded text-white"
            onClick={() => handleDelete(`${user.id}`)}
          >
            Eliminar
          </button>
        </div>
        <div className="container p-3">
          {Object.keys(user).length !== 0 ? (
            <>
              <h1>
                <b>Nombre:</b> {user.firstname} {user.lastname}
              </h1>
              <b>
                Contacto: <small>{user.email}</small>
              </b>
              <br />
              <b>Información personal:</b>
              <div className="card w-18 m-1 border-1 rounded border-gray-400 bg-gray-300 p-3 flex flex-col">
                <small>
                  <b>Cumpleaños: </b>
                  {user.birthDate}
                </small>
                <small>
                  <b>Ciudad: </b>
                  {user.address.city}
                </small>
                <small>
                  <b>Calle: </b>
                  {user.address.street}
                </small>
                <small>
                  <b>C.P.: </b>
                  {user.address.postalcode}
                </small>
              </div>
            </>
          ) : (
            <h1>El usuario que buscas no existe</h1>
          )}
        </div>
      </span>

      {Object.keys(user).length !== 0 && (
        <Modal
          isOpen={openModal}
          onRequestClose={() => setOpenModal(false)}
          style={customStyles}
          contentLabel="Editar usuario"
        >
          <form
            onSubmit={handleSubmit}
            className=" w-80 bg-zinc-200 p-2 rounded-xl"
          >
            <div className="mb-4 ">
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="firstname"
              >
                Nombre
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                id="firstname"
                {...getFieldProps("firstname")}
              />
              {touched.firstname && errors.firstname ? (
                <div className="text-red-500 text-xs italic">
                  {errors.firstname}
                </div>
              ) : null}
            </div>
            <div>
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="lastname"
              >
                Apellido
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                id="lastname"
                {...getFieldProps("lastname")}
              />
              {touched.lastname && errors.lastname ? (
                <div className="text-red-500 text-xs italic">
                  {errors.lastname}
                </div>
              ) : null}
            </div>
            <div>
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="email"
              >
                Email
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                id="email"
                {...getFieldProps("email")}
              />
              {touched.email && errors.email ? (
                <div className="text-red-500 text-xs italic">
                  {errors.email}
                </div>
              ) : null}
            </div>
            <div>
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="birthDate"
              >
                Fecha de nacimiento
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                id="birthDate"
                {...getFieldProps("birthDate")}
              />
              {touched.birthDate && errors.birthDate ? (
                <div className="text-red-500 text-xs italic">
                  {errors.birthDate}
                </div>
              ) : null}
            </div>
            <div>
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="street"
              >
                Calle
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                id="street"
                {...getFieldProps("address.street")}
              />
              {touched.address &&
              touched.address.street &&
              errors.address &&
              errors.address.street ? (
                <div className="text-red-500 text-xs italic">
                  {errors.address.street}
                </div>
              ) : null}
            </div>
            <div>
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="city"
              >
                Ciudad
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                id="city"
                {...getFieldProps("address.city")}
              />
              {touched.address &&
              touched.address.city &&
              errors.address &&
              errors.address.city ? (
                <div className="text-red-500 text-xs italic">
                  {errors.address.city}
                </div>
              ) : null}
            </div>
            <div>
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="country"
              >
                País
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                id="country"
                {...getFieldProps("address.country")}
              />
              {touched.address &&
              touched.address.country &&
              errors.address &&
              errors.address.country ? (
                <div className="text-red-500 text-xs italic">
                  {errors.address.country}
                </div>
              ) : null}
            </div>
            <div>
              <label
                className="block text-gray-700 text-sm font-bold mb-2"
                htmlFor="postalcode"
              >
                Código postal
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                id="postalcode"
                {...getFieldProps("address.postalcode")}
              />
              {touched.address &&
              touched.address.postalcode &&
              errors.address &&
              errors.address.postalcode ? (
                <div className="text-red-500 text-xs italic">
                  {errors.address.postalcode}
                </div>
              ) : null}
            </div>
            <div className="flex justify-center mt-4">
              <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-60"
                type="submit"
              >
                Crear
              </button>
            </div>
          </form>
        </Modal>
      )}
    </div>
  );
};
