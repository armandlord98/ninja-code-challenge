import { useFormik } from "formik";
import * as Yup from "yup";
import { Users } from "../interfaces/UsersInterface";
import { useContext } from "react";
import { UserContext } from "../context/users/UserContext";
import { useNavigate } from "react-router-dom";
import { GiNinjaHead } from "react-icons/gi";
import { Toaster, toast } from "sonner";

export const UserForm = () => {
  const { addUser } = useContext(UserContext);
  const navigate = useNavigate();
  const initialValues: Users = {
    id: 0,
    firstname: "",
    lastname: "",
    email: "",
    birthDate: "",
    address: {
      id: 0,
      street: "",
      city: "",
      country: "",
      postalcode: "",
    },
  };

  const { handleSubmit, getFieldProps, errors, touched } = useFormik({
    initialValues,
    onSubmit: (values) => {
      values.id = +(Math.random() * 1000000).toFixed(0);
      values.address.id = +(Math.random() * 1000000).toFixed(0);
      addUser(values);
      toast.success("Ninja creado con exito");
      setTimeout(() => {
        navigate("/");
      }, 2000);
    },
    validationSchema: Yup.object({
      firstname: Yup.string().required("Nombre es requerido"),
      lastname: Yup.string().required("Apellido es requerido"),
      email: Yup.string()
        .email("Email no valido")
        .required("Email es requerido"),
      birthDate: Yup.string().required("Fecha de nacimiento es requerido"),
      address: Yup.object({
        street: Yup.string().required("Calle es requerido"),
        city: Yup.string().required("Ciuadad es requerido"),
        country: Yup.string().required("Pais es requerido"),
        postalcode: Yup.string().required("Codigo postal es requerido"),
      }),
    }),
  });
  return (
    <div className="container w-screen h-full p-10 flex items-center flex-col">
      <Toaster position="top-right" />
      <h1 className="text-3xl text-center flex font-bold text-zinc-400 my-[2rem] sm:text-5xl">
        <GiNinjaHead />
        Crea tu Ninja
      </h1>
      <form
        onSubmit={handleSubmit}
        className=" w-80 bg-zinc-200 p-2 rounded-xl"
      >
        <div className="mb-4 ">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="firstname"
          >
            Nombre
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            id="firstname"
            {...getFieldProps("firstname")}
          />
          {touched.firstname && errors.firstname ? (
            <div className="text-red-500 text-xs italic">
              {errors.firstname}
            </div>
          ) : null}
        </div>
        <div>
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="lastname"
          >
            Apellido
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            id="lastname"
            {...getFieldProps("lastname")}
          />
          {touched.lastname && errors.lastname ? (
            <div className="text-red-500 text-xs italic">{errors.lastname}</div>
          ) : null}
        </div>
        <div>
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="email"
          >
            Email
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            id="email"
            {...getFieldProps("email")}
          />
          {touched.email && errors.email ? (
            <div className="text-red-500 text-xs italic">{errors.email}</div>
          ) : null}
        </div>
        <div>
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="birthDate"
          >
            Fecha de nacimiento
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            id="birthDate"
            {...getFieldProps("birthDate")}
          />
          {touched.birthDate && errors.birthDate ? (
            <div className="text-red-500 text-xs italic">
              {errors.birthDate}
            </div>
          ) : null}
        </div>
        <div>
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="street"
          >
            Calle
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            id="street"
            {...getFieldProps("address.street")}
          />
          {touched.address &&
          touched.address.street &&
          errors.address &&
          errors.address.street ? (
            <div className="text-red-500 text-xs italic">
              {errors.address.street}
            </div>
          ) : null}
        </div>
        <div>
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="city"
          >
            Ciudad
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            id="city"
            {...getFieldProps("address.city")}
          />
          {touched.address &&
          touched.address.city &&
          errors.address &&
          errors.address.city ? (
            <div className="text-red-500 text-xs italic">
              {errors.address.city}
            </div>
          ) : null}
        </div>
        <div>
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="country"
          >
            País
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            id="country"
            {...getFieldProps("address.country")}
          />
          {touched.address &&
          touched.address.country &&
          errors.address &&
          errors.address.country ? (
            <div className="text-red-500 text-xs italic">
              {errors.address.country}
            </div>
          ) : null}
        </div>
        <div>
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="postalcode"
          >
            Código postal
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            id="postalcode"
            {...getFieldProps("address.postalcode")}
          />
          {touched.address &&
          touched.address.postalcode &&
          errors.address &&
          errors.address.postalcode ? (
            <div className="text-red-500 text-xs italic">
              {errors.address.postalcode}
            </div>
          ) : null}
        </div>
        <div className="flex justify-center mt-4">
          <button
            id="createNinja"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-60"
            type="submit"
          >
            Crear
          </button>
        </div>
      </form>
    </div>
  );
};
