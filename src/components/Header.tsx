import { NavLink, useLocation } from "react-router-dom";
import { Route } from "../routes/routes";
import { FC } from "react";

interface Props {
  routes: Route[];
}

export const Header: FC<Props> = ({ routes }) => {
  const isHome = useLocation().pathname === "/";
  return (
    <>
      <nav className="flex flex-row justify-around items-center px-4 w-full h-16 bg-zinc-950 text-zinc-400">
        <div className="container flex justify-between items-center">
          <span className="text-2xl font-bold">
            <NavLink to="/">
              <img
                src="https://ninjatalentweb.s3.eu-west-1.amazonaws.com/uploads/2020/12/30001543/propuesta-logo-NinjaTalent-04-web-300x106-1.png"
                alt="logo propiedad de ninjatalent"
                className="h-10"
              />
            </NavLink>
          </span>
          <span className="flex flex-row justify-between items-center">
            {routes.slice(0, 2).map(({ to, name }) => (
              <ul key={to}>
                <li className="ml-4 sm:ml-8 md:ml-12 hover:text-white">
                  <NavLink
                    id={`link-${name.slice(0, 3)}`}
                    to={to}
                    className={({ isActive }) =>
                      isActive ? "text-red-600" : ""
                    }
                  >
                    {name}
                  </NavLink>
                </li>
              </ul>
            ))}
          </span>
        </div>
      </nav>
      {isHome && (
        <header className="w-screen h-[200px] lg:h-[700px] md:h-[500px] mb-[-4rem] md:mb-[-9rem] lg:mb-[-20rem]">
          <div
            className="w-full h-3/4 lg:h-1/2 object-cover bg-cover bg-center sm:h-1/2 md:h-3/4"
            style={{
              backgroundImage: `url("https://res.cloudinary.com/dy9tey0yi/image/upload/v1681423357/ninja_pmy6zk.gif")`,
              backgroundSize: "cover",
              backgroundPosition: "center",
            }}
          />
        </header>
      )}
    </>
  );
};
