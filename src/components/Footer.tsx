export const Footer = () => {
  return (
    <footer className="w-full h-24 bg-zinc-800 flex items-center justify-center">
      <p className="text-zinc-400 text-center">
        © 2023 Armandev. Todos los derechos reservados.
      </p>
    </footer>
  );
};
