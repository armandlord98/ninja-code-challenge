import { createContext } from "react";
import { Users } from "../../interfaces/UsersInterface";

// TODO: agregar el tipo de dato correcto

interface UserContext {
  state: any;
  addUser: (user: Users) => void;
  deleteUser: (id: string) => void;
  getUsers: () => void;
  getUserById: (id: string) => void;
  updateUser: (user: Users) => void;
}

export const UserContext = createContext({} as UserContext);
