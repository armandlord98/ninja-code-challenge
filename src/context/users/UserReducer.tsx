import { GET_USERS, GET_USER_BY_ID } from "./UserTypes";

interface PropsReducer {
  payload: any;
  type: string;
}
export const UserReducer = (state: any, { payload, type }: PropsReducer) => {
  switch (type) {
    case GET_USERS:
      return {
        ...state,
        users: payload,
      };
    case GET_USER_BY_ID:
      return {
        ...state,
        userById: payload,
      };

    default:
      return state;
  }
};
