import { PropsWithChildren, useReducer } from "react";
import { UserContext } from "./UserContext";
import { UserReducer as reducer } from "./UserReducer";
import { GET_USERS, GET_USER_BY_ID } from "./UserTypes";
import { Users } from "../../interfaces/UsersInterface";
import { useFirestore } from "../../hooks/useFirestore";

const UserState = ({ children }: PropsWithChildren) => {
  const { getData, getDataById, deleteData, addData, updateData } =
    useFirestore();
  interface State {
    users: Users[];
    userById: Users;
  }

  const initialState: State = {
    users: [],
    userById: {} as Users,
  };
  const [state, dispatch] = useReducer(reducer, initialState);

  const getUsers = async () => {
    const data = await getData();
    dispatch({
      type: GET_USERS,
      payload: data,
    });
  };

  const getUserById = async (id: string) => {
    const data = await getDataById(id);
    dispatch({
      type: GET_USER_BY_ID,
      payload: data,
    });
  };

  const deleteUser = (id: string) => {
    deleteData(id);
  };

  const addUser = (user: Users) => {
    addData(user);
  };

  const updateUser = (user: Users) => {
    updateData(user);
  };

  const contextData = {
    state,
    addUser,
    deleteUser,
    getUsers,
    getUserById,
    updateUser,
  };

  return (
    <UserContext.Provider value={contextData}>{children}</UserContext.Provider>
  );
};

export default UserState;
