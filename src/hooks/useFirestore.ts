import { useEffect } from "react";
import { db } from "../firebase/firebaseConfig";
import {
  collection,
  getDocs,
  setDoc,
  doc,
  getDoc,
  deleteDoc,
} from "firebase/firestore";
import { Users } from "../interfaces/UsersInterface";

export const useFirestore = () => {
  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    try {
      const querySnapshot = await getDocs(collection(db, "users"));
      const dataDB = querySnapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));
      return dataDB;
    } catch (error) {
      console.log(error);
    }
  };
  const getDataById = async (id: string) => {
    try {
      const docRef = doc(db, "users", id);
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        return docSnap.data();
      } else {
        return {};
      }
    } catch (error) {
      console.log(error);
    }
  };

  const deleteData = async (id: string): Promise<string> => {
    await deleteDoc(doc(db, "users", id));
    return "data deleted";
  };

  const addData = async (data: Users): Promise<string> => {
    await setDoc(doc(db, "users", `${data.id}`), data);
    return "data added";
  };

  const updateData = async (data: Users): Promise<string> => {
    await setDoc(doc(db, "users", `${data.id}`), data);
    return "data updated";
  };

  return {
    addData,
    deleteData,
    getData,
    getDataById,
    updateData,
  };
};
