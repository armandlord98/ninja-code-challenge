import { defineConfig } from "cypress";

export default defineConfig({
  video: false,

  retries: {
    runMode: 3,
    openMode: 3,
  },

  chromeWebSecurity: false,
  defaultCommandTimeout: 10000,
  viewportHeight: 800,
  viewportWidth: 1280,
  numTestsKeptInMemory: 0,

  e2e: {
    specPattern: "src/test/e2e/**/*.cy.js",
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
