## NINJA-CODE-CHALLENGE

Esta aplicación te ofrece una lista actualizada de los ninjas más poderosos, quienes están disponibles para llevar a cabo cualquier misión. Además, te permite crear nuevos ninjas, editar sus datos y eliminarlos si así lo deseas.

Fue desarrollada con Vite, React y TypeScript, y utiliza Firestore para almacenar los datos de cada ninja. Además, se incorporaron múltiples dependencias para hacer la aplicación más robusta, entre las cuales se incluyen Tailwind, Formik, Yup, entre otras.

Antes de desarrollar la app se realizó un diseño de baja fidelidad, el cual se puede ver en el siguiente link: [Figma](https://www.figma.com/file/2z23qzlwAkp4P09cLcbkkR/%F0%9F%A5%B7-Ninja-Code-Challenge?node-id=0%3A1&t=MCtnSONmw3LQoke1-1)

Si quieres levantar el proyecto, sigue los siguientes pasos:

Para ver el proyecto basta con clonarlo desde [github](https://gitlab.com/armandlord98/ninja-code-challenge)

Utilizar el comando nvm use para utilizar la versión de node que se encuentra en el archivo .nvmrc

```jsx
nvm use
```

Instalar dependencias:

```jsx
npm install
```

Levantar el proyecto:

```jsx
npm run dev
```

Tambien puedes verlo desplegado en el link de [vercel](https://ninja-code-challenge.vercel.app/)

Puedes probar los test e2e con el comando:

```jsx
npm run test:e2e
```

Puedes probar los test unitarios con el comando:

```jsx
npm run test:unit
```

\*Nota: Dejo las variables de entorno directas para que no tengan problemas si lo quieren levantar local.
